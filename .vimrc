:set ignorecase 
:set number relativenumber
:set nohlsearch
syntax on
:set backspace=indent,eol,start
:set nocp
set tabstop=4
set incsearch
set smartcase
set showmatch
set showmode
filetype indent on
filetype on
set pastetoggle=<F3>
if &term =~ '256color'
    " disable Background Color Erase (BCE) so that color schemes
    " render properly when inside 256-color tmux and GNU screen.
    " see also https://sunaku.github.io/vim-256color-bce.html
    set t_ut=
endif
