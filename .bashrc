#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
eval "$(starship init bash)"
alias ncmp=ncmpcpp
alias sudoedit='doas vim'

fcd() {
  cd $(find -type d | fzf)
}
open() {
  xdg-open $(find -type f | fzf)
}
getpath() {
  find -type f | fzf | sed 's/^..//' | tr -d '\n' | xclip -sel c
}
nerdpatch() {
  ~/.local/share/fontpatcher/font-patcher --fontawesome --fontawesomeextension --fontlinux --octicons --codicons --powersymbols --pomicons --powerline --powerlineextra --material --weather $1
}
alias rtorrent='rtorrent -n -o import=~/.config/rtorrent.rc'
